function [] = sampenPlot(a,SE,lengthNum,plotColors)

% Change inf to NaN
SE(SE==inf) = NaN;

% Expected a values
aExp = ones(size(SE));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Create subplot
hold on;
h=swarmchart(aExp',SE', 7.5, 'filled','MarkerFaceColor',plotColors(lengthNum,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,mean(SE,2,'omitnan')',std(SE,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
scatter(a,mean(SE,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([0 2.5]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', 0:.5:2.5); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.1f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('SampEn (bits)','FontName', 'Arial','FontSize', 12);

end