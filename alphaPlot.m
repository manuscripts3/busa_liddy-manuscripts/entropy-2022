function [] = alphaPlot(a,aEst,lengthNum,plotColors)

% Expected a values
aExp = ones(size(aEst));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Create subplot
hold on;
plot(a,a,'k-','LineWidth',1);
swarmchart(aExp',aEst', 7.5, 'filled','MarkerFaceColor',plotColors(lengthNum,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,mean(aEst,2,'omitnan')',std(aEst,0,2,'omitnan')','k','LineStyle','none','LineWidth',.75,'CapSize',0);
scatter(a,mean(aEst,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([0 2]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick',0:.25:2); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Estimated \alpha','FontName', 'Arial','FontSize', 12);

end