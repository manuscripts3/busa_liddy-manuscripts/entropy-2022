% Clear workspace and command window.
clear; clc;

if ~exist(fullfile(cd,"Results","Results_SampEn_tau.mat"),'file')

    % Add folders to path
    addpath(genpath(fullfile(cd,"SampEn")));

    % Load simulated data.
    load(fullfile(cd,"Data","Simulations_Estimation.mat"));

    % Sample entropy with different tau and baseline with tau = 1.
    E = nan(size(simData));
    Efixed = nan(size(simData));

    % Expected a
    aExp = ones(size(E));
    for i = 1:length(a)
        aExp(i,:,:) = aExp(i,:,:) .* a(i);
    end

    % SampEn hyperparameters
    m = 1; % template length
    r = .25; % tolerance
    tau = nan(size(simData)); % time delay

    % For each simulation
    for i = 1:numel(simData)

        % Current data
        y = simData{i};

        % Compute tau or set to 10 if not found.
        [p,plag] = autocorr(y,'NumLags',50);
        firstBelowE = find(p <= 1/exp(1),1,'first');
        firstMin = find(diff(p) >= 0,1,'first');
        if isempty(firstMin)
            tau(i) = 50;
        else
            tau(i) = plag(firstMin);
        end

        % Compute SampEn.
        [e1, ~, ~] = sampeng(y,m,r,tau(i),1);
        E(i) = e1(m+1);
        [e2, ~, ~] = sampeng(y,m,r,1,1);
        Efixed(i) = e2(m+1);

    end

    % Save results
    save(fullfile(cd,"Results","Results_SampEn_tau"),"E","Efixed","r","m","tau","simData","a","d","N","nSim");

else

    % Load results
    load(fullfile(cd,"Results","Results_SampEn_tau"));

    % Tau estimates by N
    tau_250 = squeeze(tau(:,1,:));
    tau_500 = squeeze(tau(:,2,:));
    tau_1000 = squeeze(tau(:,3,:));

    % SampEn estimates by N with different tau
    E_250 = squeeze(E(:,1,:));
    E_500 = squeeze(E(:,2,:));
    E_1000 = squeeze(E(:,3,:));

    % SampEn estimates by N with tau = 1
    Efixed_250 = squeeze(Efixed(:,1,:));
    Efixed_500 = squeeze(Efixed(:,2,:));
    Efixed_1000 = squeeze(Efixed(:,3,:));

    % Plot colors
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure S5 ----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 1 10 7]);
    % Panel A, Tau for short data (N = 250)
    tauPlot(a,tau_250,1,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.07, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel B, Tau for medium data (N = 500)
    tauPlot(a,tau_500,2,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.35, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel C, Tau for long data (N = 1000)
    tauPlot(a,tau_1000,3,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.63, .97, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel D, SampEn for short data (N = 250)
    sampenPlot_tau_Diff(a,E_250,Efixed_250,4,plotColors);
    annotation('textbox', [.07, .51, .2, 0],'String','D','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel E, SampEn for medium data (N = 500)
    sampenPlot_tau_Diff(a,E_500,Efixed_500,5,plotColors);
    annotation('textbox', [.35, .51, .2, 0],'String','E','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel F, SampEn for long data (N = 1000)
    sampenPlot_tau_Diff(a,E_1000,Efixed_1000,6,plotColors);
    annotation('textbox', [.63, .51, .2, 0],'String','F','FontName','Arial','FontSize',20,'LineStyle','none');

    % Print figure
    print([pwd '\Figures\FigureS5_tau'],'-dpng','-r1000');

end