% Create Figure S2 =======================================================%

% Clear workspace and command window.
clear; clc;

if exist(fullfile(cd,"Results","Results_ARFIMA_Estimation.mat"),'file')

    % Load results
    load(fullfile(pwd,"Results","Results_ARFIMA_Estimation.mat"));

     % Plot colors
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure S2 ----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 3 10 4.5]);
    % Panel A, Percent classified as nonstationary for short datasets (N=250)
    classificationPlot2(isNS,isNS2,a,1,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.07, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel B, Percent classified as nonstationary for medium datasets (N=500)
    classificationPlot2(isNS,isNS2,a,2,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.35, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel CA, Percent classified as nonstationary for long datasets (N=1000)
    classificationPlot2(isNS,isNS2,a,3,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.63, .97, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none');

    % Print figure
    print([pwd '\Figures\FigureS2_ARFIMA_DFA_Classification'],'-dpng','-r1000');

end