function [] = sampenPlot_r_WithinSE(a,SE1,SE2,SE3,SE4,SE5,plotColors)

% Change inf to NaN
SE1(SE1==inf) = NaN;
SE2(SE2==inf) = NaN;
SE3(SE3==inf) = NaN;
SE4(SE4==inf) = NaN;
SE5(SE5==inf) = NaN;

% Create subplot
hold on;
% r = .15
plot(a,mean(SE1,2,'omitnan')',"Color",plotColors(1,:),"LineWidth",1.5)
h1=scatter(a,mean(SE1,2,'omitnan')','MarkerFaceColor',plotColors(1,:),'MarkerEdgeColor','none','SizeData',15);
% r = .20
plot(a,mean(SE2,2,'omitnan')',"Color",plotColors(2,:),"LineWidth",1.5)
h2=scatter(a,mean(SE2,2,'omitnan')','MarkerFaceColor',plotColors(2,:),'MarkerEdgeColor','none','SizeData',15);
% r = .25
plot(a,mean(SE3,2,'omitnan')',"Color",plotColors(3,:),"LineWidth",1.5)
h3=scatter(a,mean(SE3,2,'omitnan')','MarkerFaceColor',plotColors(3,:),'MarkerEdgeColor','none','SizeData',15);
% r = .30
plot(a,mean(SE4,2,'omitnan')',"Color",plotColors(4,:),"LineWidth",1.5)
h4=scatter(a,mean(SE4,2,'omitnan')','MarkerFaceColor',plotColors(4,:),'MarkerEdgeColor','none','SizeData',15);
% r = .350
plot(a,mean(SE5,2,'omitnan')',"Color",plotColors(5,:),"LineWidth",1.5)
h5=scatter(a,mean(SE5,2,'omitnan')','MarkerFaceColor',plotColors(5,:),'MarkerEdgeColor','none','SizeData',15);

xlim([0 2]); ylim([0 .25]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', 0:.05:.25); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Within-simulation SE','FontName', 'Arial','FontSize', 12);
legend([h1 h2 h3 h4 h5],{'{\itr} = .15','{\itr} = .20','{\itr} = .25','{\itr} = .30','{\itr} = .35'},'orientation','vertical','location','northeast','box','off');

end