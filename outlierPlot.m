function [meanDiff,sdDiff] = outlierPlot(a,sampenDiff,N,plotColors)

% Expected a values
aExp = ones(size(sampenDiff));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Mean and SD of the percent differences
meanDiff = mean(sampenDiff,2);
sdDiff = std(sampenDiff,0,2);

% Create plot
plot([0 2],[0 0],'k','LineWidth',1);
swarmchart(aExp',sampenDiff', 7.5,'filled','MarkerFaceColor',plotColors(N,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,meanDiff',sdDiff','k','LineStyle','none','LineWidth',.75,'CapSize',0);
scatter(a,meanDiff','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([-100 100]);
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 8,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick',-100:25:100); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 10);
ylabel('Percent error (%) ','FontName', 'Arial','FontSize', 10);

end