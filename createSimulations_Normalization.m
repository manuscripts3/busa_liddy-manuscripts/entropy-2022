% Create simulated datasets using ARFIMA (0,d,0) models.

% Clear workspace and command window.
clear; clc;

% Add ARFIMA folder to path
addpath(genpath(fullfile(cd,"ARFIMA")));

% Simulation parameters include the scaling exponent(a) and corresponding
% difference orders (d), the data length (N), the number of simulations (nSim).
a = [.1:.1:.9 .99 1.1:.1:1.9]; 
d = [-.4:.1:.4 .49 -.4:.1:.4];
N = [250 500 1000 5000 10000];
nSim = 100;

% Simulated data matrix.
% To access each time series, use the indices corresponding to the 
% scaling exponent/difference order, data length, and simulation number.
%
% For example, simData{a==.5,N==250,4} provides access to the fourth simulation 
% with a=.5 and N=250. If the logical array returns all falses
% (e.g., if you search for H=.27), then this will not work.
% You can also enter the indices directly, but this becomes more confusing if
% you plan to change the range or number of values.
simData = cell(numel(a), numel(N), nSim);

% For each a/d and N
for i = 1:length(d)
    for j = 1:length(N)

        % Set the simulation parameters
        p = 0; q=0;
        sd = 1;

        % Create nSim simulations.
        for k = 1:nSim

            % Simulated data from ARFIMA(p,d,q) model
            y = ARFIMA_SIM(N(j),p,q,d(i),sd);
            if a(i) > 1
                y = cumsum(y);
            end
            
            % Set mean to zero and standard deviation to 1.
            % Note: ARFIMA_SIM has an argument to specify the standard deviation.
            % However, the returned SD is approximate. To avoid discrepancies,
            % we again normalized by the standard deviation.
            y = y - mean(y);
            y = y ./ std(y,0);

            % Add simulation
            simData{i,j,k} = y;
        end
    end
end

% Save results to .mat file.
save(fullfile(cd,'Data','Simulations_Normalization.mat'),'simData','a','d','N','nSim');