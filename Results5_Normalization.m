% Clear workspace and command window.
clear; clc;

if ~exist(fullfile(cd,"Results","Results_SampEn_Normalization.mat"),'file')

    % Add folders to path
    addpath(genpath(fullfile(cd,"SampEn")));

    % Load simulated data.
    load(fullfile(cd,"Data","Simulations_Normalization.mat"));

    % Sample entropy
    E = nan(size(simData)); % unnormalizated
    E_N = nan(size(simData)); % normalized

    % SampEn hyperparameters
    m = 1; % template length
    r = .25; % tolerance
    tau = 1; % time delay

    % Compute SampEn for each simulation.
    for i = 1:numel(simData)
        [e, e_n, ~, ~] = sampeng_norm(simData{i},m,r,tau,1);
        E(i) = e(m+1);  E_N(i) = e_n(m+1);
    end

    % Save results
    save(fullfile(cd,"Results","Results_SampEn_Normalization"),"E","E_N",...
        "r","m","tau","simData","a","d","N","nSim");

else

    % Load data
    load(fullfile(cd,"Results","Results_SampEn_Normalization"));

    % SampEn results by N
    E_250 = squeeze(E(:,1,:)); E_N_250 = squeeze(E_N(:,1,:));
    E_500 = squeeze(E(:,2,:)); E_N_500 = squeeze(E_N(:,2,:));
    E_1000 = squeeze(E(:,3,:)); E_N_1000 = squeeze(E_N(:,3,:));
    E_5000 = squeeze(E(:,4,:)); E_N_5000 = squeeze(E_N(:,4,:));
    E_10000 = squeeze(E(:,5,:)); E_N_10000 = squeeze(E_N(:,5,:));

    % Plot colors for N
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203;...
                  84,39,136;...
                  213,62,79] ./ 255;

    % Figure 6 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 .5 10 5]);
    subplot(1,2,1); hold on;
    sampenPlot2(a,E_250,E_500,E_1000,E_5000,E_10000,plotColors);
    annotation('textbox', [.06, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');
    subplot(1,2,2); hold on;
    sampenPlot2_Norm(a,E_N_250,E_N_500,E_N_1000,E_N_5000,E_N_10000,plotColors);
    annotation('textbox', [.49, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Print figure
    print([pwd '\Figures\Figure6_SampEn_Normalization'],'-dpng','-r1000');

end