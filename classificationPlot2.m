function [] = classificationPlot2(isNS1,isNS2,a,plotNum,plotColors)

% Compute percent classified as nonstationary
isNSPercent1 = nan(length(a),1);
isNSPercent2 = nan(length(a),1);
nSim = size(isNS1,3);
for i = 1:length(a)
    isNSPercent1(i) = sum(isNS1(i,plotNum,:) >= 1,3) ./ nSim;
    isNSPercent2(i) = sum(isNS2(i,plotNum,:) >= 1,3) ./ nSim;
end

% Create subplot
subplot(1,3,plotNum); hold on;
% Error bands (5 %)
plot([min(a)-.1 max(a)+.1],.95*ones(1,2),'k--','LineWidth',1);
plot([min(a)-.1 max(a)+.1],.05*ones(1,2),'k--','LineWidth',1);

% ARIMFA
h1 = plot(a',isNSPercent1,"Color",plotColors(plotNum,:),'LineWidth',1.5);
scatter(a',isNSPercent1,15,'filled',"MarkerFaceColor",plotColors(plotNum,:),"MarkerFaceAlpha",1,'LineWidth',1.5);
h2 = plot(a',isNSPercent2,"Color",plotColors(plotNum,:),'LineWidth',1.5,'LineStyle','--');
scatter(a',isNSPercent2,15,'filled',"MarkerFaceColor",plotColors(plotNum,:),"MarkerFaceAlpha",1,'LineWidth',1.5);
xlim([min(a)-.1 max(a)+.1]); ylim([-.1 1.1]);
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick', min(a)-.1:.25:max(a)+.1); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick',0:.2:1); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.1f'));
xlabel('Expected \alpha','FontName', 'Arial','FontSize', 12);
ylabel('Proportion nonstationary','FontName', 'Arial','FontSize', 12);
legend([h1 h2],{'ARFIMA','DFA'},'orientation','vertical','location','east','box','off');

end