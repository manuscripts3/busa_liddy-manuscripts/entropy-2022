function [] = sampenPlot_tau_Diff(a,E,Efixed,plotNum,plotColors)

% Change inf to NaN
E(E==inf) = NaN;
Efixed(Efixed==inf) = NaN;

% Expected a values
aExp = ones(size(E));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Mean SampEn with fixed tau
meanEfixed = mean(Efixed,2,'omitnan')';

% Mean SampEn with different tau
meanE = mean(E,2,'omitnan')';

% Create subplot;
subplot(2,3,plotNum); hold on;
plot(a,meanEfixed-meanEfixed,'r','LineWidth',1.5);
patch([a flip(a)]',vertcat(zeros(size(a))' + 2*std(Efixed,0,2,'omitnan'), zeros(size(a))' - flip(3*std(Efixed,0,2,'omitnan'))),'r','EdgeColor','none','FaceAlpha','.075');
swarmchart(aExp',E'-meanEfixed, 7.5, 'filled','MarkerFaceColor',plotColors(plotNum-3,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,meanE-meanEfixed,std(E,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
scatter(a,meanE-meanEfixed,'MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([-.75 .75]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', -.75:.25:.75); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('SampEn Difference','FontName', 'Arial','FontSize', 12);

end