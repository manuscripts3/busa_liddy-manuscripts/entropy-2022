% Clear workspace and command window.
clear; clc;

if ~exist(fullfile(cd,"Results","Results_SampEn_Outliers.mat"),'file')

    % Add folders to path
    addpath(genpath(fullfile(cd,"SampEn")));

    % Load simulated data.
    load(fullfile(cd,"Data","Simulations_Outliers.mat"));

    % SampEn estimates
    sampenBaseline = nan(size(simData));
    sampenOutliers = nan(size(simData));
    sampenAdjustedR = nan(size(simData));
    sampenOutliersRemoved = nan(size(simData));
    sampenOutliersRemovedClassified = nan(size(simData));

    % SampEn hyperparameters
    m = 1; % template length
    r = .25; % tolerance
    tau = 1; % time delay

    % For each simulation
    for i = 1:numel(simData)

        % Compute baseline SampEn.
        [e, ~, ~] = sampeng(simData{i},m,r,tau,1);
        sampenBaseline(i) = e(m+1);

        % Compute sample entropy with outliers - unadjusted r
        [e_s, ~, ~ ] = sampeng(contamSimData{i},m,r,tau,1);
        sampenOutliers(i) = e_s(m+1);

        % Compute sample entropy with outliers - adjusted r
        r_a = r * std(cleanSimData{i})/std(contamSimData{i});
        [e_a, ~, ~ ] = sampeng(contamSimData{i},m,r_a,tau,1);
        sampenAdjustedR(i) = e_a(m+1);
    
        % Compute sample entropy without outliers - unclassified)
        [e_r, ~, ~] = sampeng(cleanSimData{i},m,r,tau,1);
        sampenOutliersRemoved(i) = e_r(m+1);

        % Compute sample entropy without outliers - classified
        [e_rc, ~, ~] = sampeng(cleanSimDataClass{i},m,r,tau,1);
        sampenOutliersRemovedClassified(i) = e_rc(m+1);

    end

    % Save results
    save(fullfile(cd,"Results","Results_SampEn_Outliers"),"sampenBaseline",...
        "sampenOutliers","sampenAdjustedR","sampenOutliersRemoved",...
        "sampenOutliersRemovedClassified","a","d","N","nSim","outFound",...
        "outFreq","outMag");

else

    % Load results
    load(fullfile(pwd,"Results","Results_SampEn_Outliers.mat"));

    % Baseline SampEn separated by N
    sampenBaseline_250 = squeeze(sampenBaseline(:,1,:));
    sampenBaseline_500 = squeeze(sampenBaseline(:,2,:));
    sampenBaseline_1000 = squeeze(sampenBaseline(:,3,:));

    % Results tables
    biasResults = [];
    sdResults = [];

    % Plot colors
    plotColors = [252 141 98;...
        102 194 165;...
        141 160 203] ./ 255;

    % Figure 3 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 1 10 7]);  

    % Panels A-C, Percent error in SampEn with unadjusted r --------------%

    % SampEn with outliers separated by N
    sampenOutliers_250 = squeeze(sampenOutliers(:,1,:));
    sampenOutliers_500 = squeeze(sampenOutliers(:,2,:));
    sampenOutliers_1000 = squeeze(sampenOutliers(:,3,:));

    % Percent difference
    sampenDiff_250 = 100.*(sampenOutliers_250-sampenBaseline_250) ./ sampenBaseline_250;
    sampenDiff_500 = 100.*(sampenOutliers_500-sampenBaseline_500) ./ sampenBaseline_500;
    sampenDiff_1000 = 100.*(sampenOutliers_1000-sampenBaseline_1000) ./ sampenBaseline_1000;

    % Panel A, short data (N = 250)
    subplot(3,4,1); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_250,1,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 250','FontSize',13);
    annotation('textbox', [.075, .97, .2, 0],'String','A','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel B, medium data (N = 500)
    subplot(3,4,2); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_500,2,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 500','FontSize',13);
    annotation('textbox', [.285, .97, .2, 0],'String','B','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel C, Errors for long data (N = 1000)
    subplot(3,4,3); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_1000,3,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 1000','FontSize',13);
    annotation('textbox', [.49, .97, .2, 0],'String','C','FontName','Arial','FontSize',16,'LineStyle','none');
    
    % Row label
    annotation('textbox', [.705, .85, .2, 0],'String','Unadjusted {\itr}','FontName','Arial','FontSize',13,'LineStyle','none','FontWeight','bold');

    % Panels D-F, Percent error in SampEn with adjusted r ----------------%

    % SampEn with outliers separated by N
    sampenAdjustedSD_250 = squeeze(sampenAdjustedR(:,1,:));
    sampenAdjustedSD_500 = squeeze(sampenAdjustedR(:,2,:));
    sampenAdjustedSD_1000 = squeeze(sampenAdjustedR(:,3,:));

    % Percent difference
    sampenDiff_250 = 100.*(sampenAdjustedSD_250-sampenBaseline_250) ./ sampenBaseline_250;
    sampenDiff_500 = 100.*(sampenAdjustedSD_500-sampenBaseline_500) ./ sampenBaseline_500;
    sampenDiff_1000 = 100.*(sampenAdjustedSD_1000-sampenBaseline_1000) ./ sampenBaseline_1000;

    % Panel D, short data (N = 250)
    subplot(3,4,5); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_250,1,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.075, .67, .2, 0],'String','D','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel E, medium data (N = 500)
    subplot(3,4,6); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_500,2,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.285, .67, .2, 0],'String','E','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel F, Errors for long data (N = 1000)
    subplot(3,4,7); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_1000,3,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.49, .67, .2, 0],'String','F','FontName','Arial','FontSize',16,'LineStyle','none');

    % Row label
    annotation('textbox', [.705, .56, .2, 0],'String','Adjusted {\itr}','FontName','Arial','FontSize',13,'LineStyle','none','FontWeight','bold');

    % Panels G-I, Percent error in SampEn with outliers removed ----------%

    % SampEn with outliers separated by N
    sampenOutliersRemoved_250 = squeeze(sampenOutliersRemoved(:,1,:));
    sampenOutliersRemoved_500 = squeeze(sampenOutliersRemoved(:,2,:));
    sampenOutliersRemoved_1000 = squeeze(sampenOutliersRemoved(:,3,:));

    % Percent difference
    sampenDiff_250 = 100.*(sampenOutliersRemoved_250-sampenBaseline_250) ./ sampenBaseline_250;
    sampenDiff_500 = 100.*(sampenOutliersRemoved_500-sampenBaseline_500) ./ sampenBaseline_500;
    sampenDiff_1000 = 100.*(sampenOutliersRemoved_1000-sampenBaseline_1000) ./ sampenBaseline_1000;

    % Panel G, short data (N = 250)
    subplot(3,4,9); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_250,1,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.075, .37, .2, 0],'String','G','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel H, medium data (N = 500)
    subplot(3,4,10); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_500,2,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.285, .37, .2, 0],'String','H','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel I, Errors for long data (N = 1000)
    subplot(3,4,11); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_1000,3,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.49, .37, .2, 0],'String','I','FontName','Arial','FontSize',16,'LineStyle','none');

    % Row label
    annotation('textbox', [.705, .27, .2, 0],'String','Outliers removed','FontName','Arial','FontSize',13,'LineStyle','none','FontWeight','bold');

    % Print figure
    print([pwd '\Figures\Figure3_SampEn_Outliers'],'-dpng','-r1000');

    % Tables -------------------------------------------------------------%
    % Bias and SD
    varNames = {'Expected a','Unadjusted, N=250','Unadjusted, N=500','Unadjusted, N=1000',...
                'Adjusted, N=250','Adjusted, N=500','Adjusted, N=1000'...
                'Removed, N=250','Removed, N=500','Removed, N=1000'};

    biasResults = horzcat(a',biasResults);
    BiasTable = array2table(biasResults,'VariableNames',varNames);
    sdResults = horzcat(a',sdResults); 
    SDTable = array2table(sdResults,'VariableNames',varNames);

    % Percent of outliers removed less than the expected amount.
    varNames = {'Expected a','Percent removed (N = 250)','Percent removed (N = 500)','Percent removed (N = 1000)'};
 
    outFound_250 = squeeze(outFound(:,1,:)); percentExpOut_250 = sum(outFound_250 <= 3,2)./nSim;
    outFound_500 = squeeze(outFound(:,2,:)); percentExpOut_500 = sum(outFound_500 <= 5,2)./nSim;
    outFound_1000 = squeeze(outFound(:,3,:)); percentExpOut_1000 = sum(outFound_1000 <= 10,2)./nSim;

    percentRemovedResults = horzcat(a',percentExpOut_250,percentExpOut_500,percentExpOut_1000);
    PercentRemovedTable = array2table(percentRemovedResults,'VariableNames',varNames);

    % Figure 4 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 2 10 5]);

    % Results tables
    biasResults = [];
    sdResults = [];

    % Panels A-C, Percent error in SampEn with outliers removed ----------%
    % Datasets were NOT CLASSIFIED prior to outlier removal.

    % SampEn with outliers separated by N
    sampenOutliersRemoved_250 = squeeze(sampenOutliersRemoved(:,1,:));
    sampenOutliersRemoved_500 = squeeze(sampenOutliersRemoved(:,2,:));
    sampenOutliersRemoved_1000 = squeeze(sampenOutliersRemoved(:,3,:));

    % Percent difference
    sampenDiff_250 = 100.*(sampenOutliersRemoved_250-sampenBaseline_250) ./ sampenBaseline_250;
    sampenDiff_500 = 100.*(sampenOutliersRemoved_500-sampenBaseline_500) ./ sampenBaseline_500;
    sampenDiff_1000 = 100.*(sampenOutliersRemoved_1000-sampenBaseline_1000) ./ sampenBaseline_1000;

    % Panel A, short data (N = 250)
    subplot(2,4,1); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_250,1,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 250','FontSize',13);
    annotation('textbox', [.075, .98, .2, 0],'String','A','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel B, medium data (N = 500)
    subplot(2,4,2); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_500,2,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 500','FontSize',13);
    annotation('textbox', [.285, .98, .2, 0],'String','B','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel C, Errors for long data (N = 1000)
    subplot(2,4,3); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_1000,3,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    title('{\itN} = 1000','FontSize',13);
    annotation('textbox', [.49, .98, .2, 0],'String','C','FontName','Arial','FontSize',16,'LineStyle','none');
    
    % Row label
    annotation('textbox', [.705, .8, .2, 0],'String','Unclassified','FontName','Arial','FontSize',13,'LineStyle','none','FontWeight','bold');

    % Panels D-F, Percent error in SampEn with outliers removed ----------%
    % Datasets were CLASSIFIED prior to outlier removal.

    % SampEn with outliers separated by N
    sampenOutliersRemovedClassified_250 = squeeze(sampenOutliersRemovedClassified(:,1,:));
    sampenOutliersRemovedClassified_500 = squeeze(sampenOutliersRemovedClassified(:,2,:));
    sampenOutliersRemovedClassified_1000 = squeeze(sampenOutliersRemovedClassified(:,3,:));

    % Percent difference
    sampenDiff_250 = 100.*(sampenOutliersRemovedClassified_250-sampenBaseline_250) ./ sampenBaseline_250;
    sampenDiff_500 = 100.*(sampenOutliersRemovedClassified_500-sampenBaseline_500) ./ sampenBaseline_500;
    sampenDiff_1000 = 100.*(sampenOutliersRemovedClassified_1000-sampenBaseline_1000) ./ sampenBaseline_1000;

    % Panel D, short data (N = 250)
    subplot(2,4,5); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_250,1,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.075, .51, .2, 0],'String','D','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel E, medium data (N = 500)
    subplot(2,4,6); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_500,2,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.285, .51, .2, 0],'String','E','FontName','Arial','FontSize',16,'LineStyle','none');

    % Panel F, Errors for long data (N = 1000)
    subplot(2,4,7); hold on;
    [meanDiff,sdDiff] = outlierPlot(a,sampenDiff_1000,3,plotColors);
    biasResults = horzcat(biasResults, meanDiff); sdResults = horzcat(sdResults, sdDiff);
    annotation('textbox', [.49, .51, .2, 0],'String','F','FontName','Arial','FontSize',16,'LineStyle','none');

    % Row label
    annotation('textbox', [.705, .32, .2, 0],'String','Classified','FontName','Arial','FontSize',13,'LineStyle','none','FontWeight','bold');

    % Print figure
    print([pwd '\Figures\Figure4_SampEn_OutliersClassified'],'-dpng','-r1000');

    % Tables -------------------------------------------------------------%
    % Bias and SD
    varNames = {'Expected a','Unclassified, N=250','Unclassified, N=500','Unclassified, N=1000',...
                'Classified, N=250','Classified, N=500','Classified, N=1000'};

    biasResults = horzcat(a',biasResults);
    BiasTable2 = array2table(biasResults,'VariableNames',varNames);
    sdResults = horzcat(a',sdResults); 
    SDTable2 = array2table(sdResults,'VariableNames',varNames);

end