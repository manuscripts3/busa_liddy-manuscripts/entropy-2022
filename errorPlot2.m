function [] = errorPlot2(a,aEst,plotNum,lengthN,plotColors)

% Expected a values
aExp = ones(size(aEst));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Create subplot;
subplot(2,3,plotNum);
plot(a,a-a,'k-','LineWidth',.5); hold on;
swarmchart(aExp',aEst'-aExp', 7.5, 'filled','MarkerFaceColor',plotColors(lengthN,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,mean(aEst,2)'-a,std(aEst,0,2)','k','LineStyle','none','LineWidth',.75,'CapSize',0);
scatter(a,mean(aEst,2)'-a,'MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([-0.25 0.25]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick',-.20:.1:.20); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Estimated \alpha error','FontName', 'Arial','FontSize', 12);

end