% Clear workspace and command window.
clear; clc;

if ~exist(fullfile(cd,"Results","Results_SampEn_m.mat"),'file')

    % Add folders to path
    addpath(genpath(fullfile(cd,"SampEn")));

    % Load simulated data.
    load(fullfile(cd,"Data","Simulations_Estimation.mat"));

    % Sample entropy for m = 1,2,3
    E1 = nan(size(simData)); SE1 = nan(size(simData));
    E2 = nan(size(simData)); SE2 = nan(size(simData));
    E3 = nan(size(simData)); SE3 = nan(size(simData));

    % SampEn hyperparameters
    m = 3; % template length
    r = .2; % tolerance

    % Compute SampEn for each simulation.
    for i = 1:numel(simData)
        [e, se, ~, ~] = sampen(simData{i},m+1,r,1,0,1);
        E1(i) = e(m-1); SE1(i) = se(m-1);
        E2(i) = e(m);  SE2(i) = se(m);
        E3(i) = e(m+1); SE3(i) = se(m+1);
    end

    % Save results
    save(fullfile(cd,"Results","Results_SampEn_m"),"E1","E2","E3",...
        "SE1","SE2","SE3","r","m","tau","simData","a","d","N","nSim");

else

    % Load data
    load(fullfile(cd,"Results","Results_ARFIMA_Estimation.mat"));
    load(fullfile(cd,"Results","Results_SampEn_m.mat"));

    % SampEn for each m and N
    E1_250 = squeeze(E1(:,1,:)); E2_250 = squeeze(E2(:,1,:)); E3_250 = squeeze(E3(:,1,:));
    E1_500 = squeeze(E1(:,2,:)); E2_500 = squeeze(E2(:,2,:)); E3_500 = squeeze(E3(:,2,:));
    E1_1000 = squeeze(E1(:,3,:)); E2_1000 = squeeze(E2(:,3,:)); E3_1000 = squeeze(E3(:,3,:));

    % SampEn standard error for each m and N
    SE1_250 = squeeze(SE1(:,1,:)); SE2_250 = squeeze(SE2(:,1,:)); SE3_250 = squeeze(SE3(:,1,:));
    SE1_500 = squeeze(SE1(:,2,:)); SE2_500 = squeeze(SE2(:,2,:)); SE3_500 = squeeze(SE3(:,2,:));
    SE1_1000 = squeeze(SE1(:,3,:)); SE2_1000 = squeeze(SE2(:,3,:)); SE3_1000 = squeeze(SE3(:,3,:));

    % Plot colors for m
    plotColors = [127,205,187;...
                  29,145,192;...
                  37,52,148] ./ 255;

    % Figure S3 ----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 1 10 7.5]);
      
    % Panel A, SampEn for short data (N = 250)
    subplot(2,3,1);
    sampenPlot_m_WithinSE(a,SE1_250,SE2_250,SE3_250,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.06, .965, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel B, SampEn for medium data (N = 500)
    subplot(2,3,2);
    sampenPlot_m_WithinSE(a,SE1_500,SE2_500,SE3_500,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.34, .965, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel C, SampEn for long data (N = 1000)
    subplot(2,3,3);
    sampenPlot_m_WithinSE(a,SE1_1000,SE2_1000,SE3_1000,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.62, .965, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none'); 

    % Panel D, SampEn for short data (N = 250)
    subplot(2,3,4);
    sampenPlot_m_BetweenSE(a,E1_250,E2_250,E3_250,plotColors);
    annotation('textbox', [.06, .49, .2, 0],'String','D','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel E, SampEn for medium data (N = 500)
    subplot(2,3,5);
    sampenPlot_m_BetweenSE(a,E1_500,E2_500,E3_500,plotColors);
    annotation('textbox', [.34, .49, .2, 0],'String','E','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel F, SampEn for long data (N = 1000)
    subplot(2,3,6);
    sampenPlot_m_BetweenSE(a,E1_1000,E2_1000,E3_1000,plotColors);
    annotation('textbox', [.62, .49, .2, 0],'String','F','FontName','Arial','FontSize',20,'LineStyle','none'); 
  
    % Print figure
    print([pwd '\Figures\FigureS3_m'],'-dpng','-r1000');

end