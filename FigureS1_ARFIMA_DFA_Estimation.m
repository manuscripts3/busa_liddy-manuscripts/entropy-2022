% Create Figure S1 =======================================================%

% Clear workspace and command window.
clear; clc;

if exist(fullfile(cd,"Results","Results_ARFIMA_Estimation.mat"),'file')

    % Load results
    load(fullfile(pwd,"Results","Results_ARFIMA_Estimation.mat"));

    % ARFIMA results by N
    aEst_ARFIMA_250 = squeeze(aEst(:,1,:));
    aEst_ARFIMA_500 = squeeze(aEst(:,2,:));
    aEst_ARFIMA_1000 = squeeze(aEst(:,3,:));

    % DFA results by N
    aEst_DFA_250 = squeeze(aEst2(:,1,:));
    aEst_DFA_500 = squeeze(aEst2(:,2,:));
    aEst_DFA_1000 = squeeze(aEst2(:,3,:));

    % Plot colors
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure S1 ----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 2 10 7]);
    % ARFIMA
    % Panel A, ARFIMA errors for short data (N = 250)
    errorPlot2(a,aEst_ARFIMA_250,1,1,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.08, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel B, ARFIMA errors for medium data (N = 500)
    errorPlot2(a,aEst_ARFIMA_500,2,2,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.36, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel C, ARFIMA errors for long data (N = 1000)
    errorPlot2(a,aEst_ARFIMA_1000,3,3,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.64, .97, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none');

    % ARFIMA label
    annotation('textbox', [.905, .78, .2, 0],'String','ARFIMA','FontName','Arial','FontSize',15,'LineStyle','none','FontWeight','bold');

    % DFA
    % Panel D, DFA errors for short data (N = 250)
    errorPlot2(a,aEst_DFA_250,4,1,plotColors);
    annotation('textbox', [.08, .5, .2, 0],'String','D','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel E, DFA errors for medium data (N = 500)
    errorPlot2(a,aEst_DFA_500,5,2,plotColors);
    annotation('textbox', [.36, .5, .2, 0],'String','E','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel F, DFA errors for long data (N = 1000)
    errorPlot2(a,aEst_DFA_1000,6,3,plotColors);
    annotation('textbox', [.64, .5, .2, 0],'String','F','FontName','Arial','FontSize',20,'LineStyle','none');

    % DFA label
    annotation('textbox', [.905, .31, .2, 0],'String','DFA','FontName','Arial','FontSize',15,'LineStyle','none','FontWeight','bold');

    % Print figure
    print([pwd '\Figures\FigureS1_ARFIMA_DFA_Estimation'],'-dpng','-r1000');

end