function [] = sampenPlot2_Norm(a,E1,E2,E3,E4,E5,plotColors)

% Change inf to NaN
E1(E1==inf) = NaN;
E2(E2==inf) = NaN;
E3(E3==inf) = NaN;
E4(E4==inf) = NaN;
E5(E5==inf) = NaN;

% Expected a values
aExp = ones(size(E1));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% N = 250
h1=swarmchart(aExp',E1', 7.5, 'filled','MarkerFaceColor',plotColors(1,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
% errorbar(a,mean(E1,2,'omitnan')',std(E1,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
% scatter(a,mean(E1,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
% N = 500
h2=swarmchart(aExp',E2', 7.5, 'filled','MarkerFaceColor',plotColors(2,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
% errorbar(a,mean(E2,2,'omitnan')',std(E2,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
% scatter(a,mean(E2,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
% N = 1000
h3=swarmchart(aExp',E3', 7.5, 'filled','MarkerFaceColor',plotColors(3,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
% errorbar(a,mean(E3,2,'omitnan')',std(E3,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
% scatter(a,mean(E3,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
% N = 5000
h4=swarmchart(aExp',E4', 7.5, 'filled','MarkerFaceColor',plotColors(4,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
% errorbar(a,mean(E4,2,'omitnan')',std(E4,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
% scatter(a,mean(E4,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
% N = 10000
h5=swarmchart(aExp',E5', 7.5, 'filled','MarkerFaceColor',plotColors(5,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
% errorbar(a,mean(E5,2,'omitnan')',std(E5,0,2,'omitnan')','k','LineStyle','none','LineWidth',1,'CapSize',0);
% scatter(a,mean(E5,2,'omitnan')','MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);

xlim([0 2]); ylim([0 .25]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', 0:.05:.25); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Normalized SampEn','FontName', 'Arial','FontSize', 12);
legend([h1(1,1) h2(1,1) h3(1,1) h4(1,1) h5(1,1)],{'{\itN} = 250','{\itN} = 500','{\itN} = 1000','{\itN} = 5000','{\itN} = 10000'},'orientation','vertical','location','northeast','box','off');

end