% Contaminate simulated datasets with outliers.

% Clear workspace and command window.
clear; clc;

% Add folders to path
addpath(genpath(fullfile(cd,"sampen")));

% Load data.
load(fullfile(pwd,"Results","Results_ARFIMA_Estimation.mat"));

% Contaminated and cleaned data
contamSimData = cell(size(simData));
cleanSimData = cell(size(simData));
cleanSimDataClass = cell(size(simData));

% Number of outliers found
outFound = nan(size(simData));

% Outlier parameters
outFreq = .01;
outMag = 3;

% For each simulation
for i = 1:numel(simData)

    % Current data
    y = simData{i};

    % Create outliers using specified parameters and add to data.
    nOut = round(length(y)*outFreq);
    out = zeros(size(y));
    outIdx = randi(length(y),1,nOut)';
    outAmp = outMag .* range(y) .* normrnd(0,1,1,nOut)';
    out(outIdx) = out(outIdx) + outAmp;
    y_out = y + out;
    contamSimData{i} = y_out;

    % Identify and remove outliers using 5*MAD
    isOut = abs(y_out) > 5 .* mad(y_out);
    y_rem = y_out(~isOut);
    cleanSimData{i} = y_rem;

    % Outliers found
    outFound(i) = sum(isOut);

    % Classify signal and remove outliers using 5*MAD
    if isNS(i) > 0
        y_out_diff = diff(y_out);
        isOut = abs(y_out_diff) > 5 .* mad(y_out_diff);
        outIdx = find(diff(isOut) == -1);
        y_rem = y_out; y_rem(outIdx) = [];
        cleanSimDataClass{i} = y_rem;
        outFound(i) = length(outIdx);
    else
        cleanSimDataClass{i} = cleanSimData{i};
    end
end

% Save results.
save(fullfile(cd,"Data","Simulations_Outliers.mat"),"contamSimData",...
    "cleanSimData","cleanSimDataClass","outFreq","outMag","outFound",...
    "a","d","N","nSim","simData","isNS");