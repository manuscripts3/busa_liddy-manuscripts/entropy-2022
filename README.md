# Entropy-2023

## Description
This GitLab project contains the MATLAB scripts and functions to reproduce the data, analyses, and figures published in
Liddy, J.; Busa, M. Considerations for Applying Entropy Methods to Temporally Correlated Stochastic Datasets. Entropy 2023, 25, #. https://doi.org/10.3390/#####.

## Usage
We did our best to make this repository user friendly, but hope to improve our approach for future publications.
First, you'll notice the project contains a number of folders. Three folders contain analytical techniques examined 
in the paper or the supplmental materials: ARFIMA, sampen, and DFA. There are other folders to store the simulated 
datasets (Data), results (Results), and figures (Figures).

There various MATLAB scripts and functions in the main folder. To start, you can (re)produce the simulated datasets 
by running the different createSimulations scripts, which add any necessary file paths and save the simulations in a 
.mat file. Subsequently, you (re)run the data analyses to obtain different sets of results using the Results# scripts. 
These scripts conduct the analyses reported in the paper and (re)produce the figures/tables found in the main body of 
the paper. To (re)produce the supplmental figures, you can run the Figure S# scripts. To create the figures, we 
created a number of different plotting functions (e.g., errorPlot.m). Some of these functions have different versions 
depending on how the data were presented.

## Project status
Complete. No changes will be made.

## Contributing
No contributions.

## Authors
This repository was created and is maintained by Joshua Liddy (jliddy@umass.edu).

## License
CCO 1.0 Universal