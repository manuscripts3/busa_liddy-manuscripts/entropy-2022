function [e, se, A, B] = sampen(y, M, r, sflag, cflag, vflag)
% Computes SampEn for input signal, y, with maximum template length, m, and
% matching tolerance, r. Optional outputs include the standard error and 
% the number of matches for m-1 and m, where m = 1,2,...,M
%
% Arguments
% y - input signal vector (n x 1)
% m - maximum template length (default M=2)
% r - matching tolerance (default r=.2)
% sflag - flag to standardize signal by setting to 1 (default yes/sflag=1) 
% cflag - flag to use fast C code by setting to 1 (default no/cflag=0) 
% vflag - flag to calculate standard errors by setting to 1 (default no/vflag=0) 
%
% Returns
% e - SampEn estimates for m=0,1,...,M-1
% se - standard error estimates for m=0,1,...,M-1
% A number of matches for m=1,...,M
% B number of matches for m=0,...,M-1
% ======================================================================= %

% Use default values for variables not provided.
if ~exist('M','var')     || isempty(M), M=2; end
if ~exist('r','var')     || isempty(r), r=.2; end
if ~exist('sflag','var') || isempty(sflag), sflag=1; end
if ~exist('cflag','var') || isempty(cflag), cflag=0; end
if ~exist('vflag','var') || isempty(vflag), vflag=0; end

% Converts input signal to vector.
y = y(:);

% Length of input signal
n = length(y);

% Standardize signal to zero mean and unit variance.
if sflag == 1
   y=y-mean(y);
   y=y./std(y);
end

% Compute standard error if requested.
if vflag == 1
    se = sampense(y,M,r);
else
    se=[];
end

% Use fast C code if requested.
if cflag == 1
   [match,~] = cmatches(y,n,r);
   match = double(match);
else   
   [e,A,B] = sampenc(y,M,r);
   return
end

% Compute number of matches for m-1 (B) and m (A) and estimate SampEn (e).
k=length(match);
if k<M
   match((k+1):M)=0;
end
N=n*(n-1)/2;
A=match(1:M);
B=[N;A(1:(M-1))];
p=A./B;
e=-log(p);

end % End sampen.