## SampEn

## Description
Use sampen.m to specify the input data (y), maximum template length (M), 
and matching tolerance (r). There are also optional arguments to 
standardize the signal (sflag), use the fast C code (cflag), and estimate
the standard error (vflag).

Use sampeng.m to use the generalized version of SampEn with the addition
hyperparameter of time delay, tau.

Note: The fast C code is currently not working so cflag is set to 0 by 
default. Setting cflag to 1 will cause the program to throw an error.

Because the fast C code option is not available, sampenc.m is called to 
compute sampen(y,M,r).

There is also cross_sampen.m to measure the regularity shared across two 
input datasets (x and y).

## Usage
None. Currently under construction.

## Support
Contact jliddy@umass.edu with issues.

## Roadmap
Build example_sampen.m for basic use case examples.
No bugs identified.
No features planned.

## Authors and acknowledgment
These codes were originally downloaded from PhysioNet.

Goldberger, A., Amaral, L., Glass, L., Hausdorff, J., Ivanov, P. C., Mark, R., ... & Stanley, H. E. (2000). PhysioBank, PhysioToolkit, and PhysioNet: Components of a new research resource for complex physiologic signals. Circulation [Online]. 101 (23), pp. e215–e220.

Comments were added by Josh Liddy for clarity and minor changes were made
to the data standardization sections. Last edited September 8, 2022.

## License
GNU General Public License 2.0
