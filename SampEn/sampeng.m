function [e, A, B] = sampeng(y,M,r,tau,nflag)
% Computes SampEn for input signal, y, with maximum template length, m, and
% matching tolerance, r, and time delay, tau. Optional outputs include the 
% number of matches for m-1 and m, where m = 0,2,...,M-1
%
% Arguments
% y - input signal vector (n x 1)
% M - maximum template length (default M=2)
% r - matching tolerance (default r=.2)
% tau - time delay (default tau=1)
% nflag - flag to normalize signal by setting to 1 (default yes/sflag=1) 
%
% Returns
% e - SampEn estimates for m=0,1,...,M
% A number of matches for m=1,...,M+1
% B number of matches for m=0,...,M
% ======================================================================= %

% Use default values for variables not provided.
if ~exist('M','var')     || isempty(M), M=2; end
if ~exist('r','var')     || isempty(r), r=.2; end
if ~exist('tau','var')   || isempty(tau), tau=1; end
if ~exist('nflag','var') || isempty(nflag), nflag = 1; end

% Converts data to vector
y = y(:);

% Normalize signal to 0 mean and 1 SD 
if nflag == 1
    y = (y - mean(y))./std(y);
end

% Length of data
N=length(y);

% Match tracking arrays
run=zeros(1,N);
lastrun=zeros(1,N);

% Match counts
A=zeros(1,M+1);
B=zeros(1,M+1);

% Estimate SampEn
for i=1:(N-1)
   nj=N-i-(tau-1);
   y1=y(i);
   for jj=1:nj
      j=jj+i+(tau-1);
      if abs(y(j)-y1)<r
         run(jj)=lastrun(jj)+1;
         M1=min(M+1,run(jj));
         for m=1:M1           
            A(m)=A(m)+1;
            if j<N
               B(m)=B(m)+1;
            end            
         end
      else
         run(jj)=0;
      end      
   end
   for j=1:nj
      lastrun(j)=run(j);
   end
end
N2 = N*(N-1)/2;
B = [N2 B(1:M)];
p = A./B;
e = -log(p);
end