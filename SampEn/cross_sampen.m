function [e,A,B]=cross_sampen(x,y,M,r,sflag)
% Computes crossSampEn for input signals, x and y, with maximum template 
% length, m, and matching tolerance, r. Optional outputs include the number
% of matches for m-1 and m, where m = 1,2,...,M.
%
% Arguments
% x,y - input signal vectors (n x 1)
% m - maximum template length (default M=2)
% r - matching tolerance (default r=.2)
% sflag - flag to standardize signal by setting to 1 (default yes/sflag=1) 
%
% Returns
% e - crossSampEn estimates for m=0,1,...,M-1
% A number of matches for m=1,...,M
% B number of matches for m=0,...,M-1
%=========================================================================%

% Use default values for variables not provided.
if ~exist('M','var')     || isempty(M), M=2; end
if ~exist('r','var')     || isempty(r), r=.2; end
if ~exist('sflag','var') || isempty(sflag), sflag=1; end

% Converts input signals to vectors.
y=y(:);
x=x(:);

% Length of input signals
ny = length(y);
nx = length(x);

% Standardize signals to zero mean and unit variance.
if sflag == 1
   y=y-mean(y);
   y=y./std(x);
   x=x-mean(x);
   x=x./std(x);   
end

% Computes number of matches for m-1 (B) and m (A) and estimates crossSampEn (e).
lastrun=zeros(nx,1);
run=zeros(nx,1);
A=zeros(M,1);
B=zeros(M,1);
for i=1:ny
   for j=1:nx
      if abs(x(j)-y(i))<r
         run(j)=lastrun(j)+1;
         M1=min(M,run(j));
         for m=1:M1           
            A(m)=A(m)+1;
            if (i<ny)&&(j<nx)
               B(m)=B(m)+1;
            end            
         end
      else
         run(j)=0;
      end      
   end
   for j=1:nx
      lastrun(j)=run(j);
   end
end
N=ny*nx;
B=[N;B(1:(M-1))];
p=A./B;
e=-log(p);

end % End function.