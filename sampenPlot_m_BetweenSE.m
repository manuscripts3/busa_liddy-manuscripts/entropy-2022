function [] = sampenPlot_m_BetweenSE(a,E1,E2,E3,plotColors)

% Change inf to NaN
E1(E1==inf) = NaN;
E2(E2==inf) = NaN;
E3(E3==inf) = NaN;

% Create subplot
hold on;
% m = 1
plot(a,std(E1,0,2,'omitnan')'./sqrt(size(E1,2)),"Color",plotColors(1,:),"LineWidth",1.5)
h1=scatter(a,std(E1,0,2,'omitnan')'./sqrt(size(E1,2)),'MarkerFaceColor',plotColors(1,:),'MarkerEdgeColor','none','SizeData',15);
% m = 2
plot(a,std(E2,0,2,'omitnan')'./sqrt(size(E2,2)),"Color",plotColors(2,:),"LineWidth",1.5)
h2=scatter(a,std(E2,0,2,'omitnan')'./sqrt(size(E2,2)),'MarkerFaceColor',plotColors(2,:),'MarkerEdgeColor','none','SizeData',15);
% m = 3
plot(a,std(E3,0,2,'omitnan')'./sqrt(size(E3,2)),"Color",plotColors(3,:),"LineWidth",1.5)
h3=scatter(a,std(E3,0,2,'omitnan')'./sqrt(size(E3,2)),'MarkerFaceColor',plotColors(3,:),'MarkerEdgeColor','none','SizeData',15);

xlim([0 2]); ylim([0 .1]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', 0:.02:.1); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.2f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Between-simulation SE','FontName', 'Arial','FontSize', 12);
legend([h1 h2 h3],{'{\itm} = 1','{\itm} = 2','{\itm} = 3'},'orientation','vertical','location','northeast','box','off');

end