% Fit ARFIMA(0,d,0) models to estimate the expected difference order (d) 
% and scaling exponent(a) to assess classification accuracy.

% Clear workspace and command window.
clear; clc;

% Add folders to path
addpath(genpath(fullfile(cd,"ARFIMA")));

if ~exist(fullfile(cd,"Results","Results_ARFIMA_Classification.mat"),'file')

    % Load simulated data
    load(fullfile(pwd,'Data',"Simulations_Classification.mat"));

    % Difference order, d, and scaling exponent, a.
    dEst = nan(size(simData));
    aEst = nan(size(simData));

    % Indicates whether data is nonstationary
    isNS = zeros(size(simData));

    % Obtain parameter estimates for ARFIMA(0,d,0) model.
    for i = 1:numel(simData)
        y = simData{i};
        model = arfima_estimate(y,'FWHI',[0 0]);
        while model.d(1) > 0.499
            isNS(i) = isNS(i) + 1;
            y = diff(y);
            model = arfima_estimate(y,'FWHI',[0 0]);
        end
        dEst(i) = model.d(1);
        if isNS(i)
            dEst(i) = dEst(i) + isNS(i);
        end
        aEst(i) = (2*dEst(i) + 1) / 2;
    end

    % Save results
    save(fullfile(cd,"Results","Results_ARFIMA_Classification"),'aEst','dEst','isNS','a','d','N','nSim','simData');

else

    % Plot colors
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure 2 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[4 3 8 4.5]);
    % Panel A, Percent classified as nonstationary from estimation sims
    load(fullfile(pwd,"Results","Results_ARFIMA_Estimation.mat"));
    classificationPlot(isNS,a,1,plotColors);
    title('Initial analysis');
    annotation('textbox', [.05, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel B, Percent classified as nonstationary from classification sims
    load(fullfile(pwd,"Results","Results_ARFIMA_Classification.mat"));
    classificationPlot(isNS,a,2,plotColors);
    title('Follow-up analysis');
    annotation('textbox', [.49, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Print figure
    print([pwd '\Figures\Figure2_ARFIMA_Classification'],'-dpng','-r1000');

end