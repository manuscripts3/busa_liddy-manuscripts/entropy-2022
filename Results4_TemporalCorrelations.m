% Clear workspace and command window.
clear; clc;

if ~exist(fullfile(cd,"Results","Results_SampEn_TemporalCorrelations.mat"),'file')

    % Add folders to path
    addpath(genpath(fullfile(cd,"SampEn")));

    % Load simulated data.
    load(fullfile(cd,"Data","Simulations_Estimation.mat"));

    % Sample entropy estimates
    E = nan(size(simData));
    SE = nan(size(simData));

    % SampEn hyperparameters
    m = 1; % template length
    r = .25; % tolerance
    tau = 1; % time delay

    % Compute SampEn for each simulation.
    for i = 1:numel(simData)
        [e, ~, ~] = sampeng(simData{i},m,r,tau,1);
        E(i) = e(m+1);
    end

    % Save results
    save(fullfile(cd,"Results","Results_SampEn_TemporalCorrelations"),"E",...
        "r","m","tau","simData","a","d","N","nSim");

else

    % Load data
    load(fullfile(cd,"Results","Results_ARFIMA_Estimation"));
    load(fullfile(cd,"Results","Results_SampEn_TemporalCorrelations"));

    % Scaling exponent, a, results by N
    aEst_250 = squeeze(aEst(:,1,:));
    aEst_500 = squeeze(aEst(:,2,:));
    aEst_1000 = squeeze(aEst(:,3,:));

    % SampEn results by N
    E_250 = squeeze(E(:,1,:));
    E_500 = squeeze(E(:,2,:));
    E_1000 = squeeze(E(:,3,:));

    % Plot colors for N
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure 5 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 .5 10 7]);
    % Panel A, Scaling exponent for short data (N = 250)
    subplot(2,3,1);
    alphaPlot(a,aEst_250,1,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.06, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel B, Scaling exponent for medium data (N = 500)
    subplot(2,3,2);
    alphaPlot(a,aEst_500,2,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.345, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel C, Scaling exponent for long data (N = 1000)
    subplot(2,3,3);
    alphaPlot(a,aEst_1000,3,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.625, .97, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none');
    
    % Panel D, SampEn for short data (N = 250)
    subplot(2,3,4);
    sampenPlot(a,E_250,1,plotColors);
    annotation('textbox', [.06, .5, .2, 0],'String','D','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel E, SampEn for medium data (N = 500)
    subplot(2,3,5);
    sampenPlot(a,E_500,2,plotColors);
    annotation('textbox', [.345, .5, .2, 0],'String','E','FontName','Arial','FontSize',20,'LineStyle','none');
    % Panel F, SampEn for long data (N = 1000)
    subplot(2,3,6);
    sampenPlot(a,E_1000,3,plotColors);
    annotation('textbox', [.625, .5, .2, 0],'String','F','FontName','Arial','FontSize',20,'LineStyle','none'); 

    % Print figure
    print([pwd '\Figures\Figure5_Alpha_SampEn'],'-dpng','-r1000');

    % Table 3 ------------------------------------------------------------%
    % Bias and standard deviation
    results = horzcat(a',mean(E_250,2),std(E_250,0,2),mean(E_500,2),std(E_500,0,2),mean(E_1000,2),std(E_1000,0,2));
    varNames = {'Expected a','Mean N=250','Mean N=500','Mean N=1000','SD N=250','SD N=500','SD N=1000'};
    Table3 = array2table(results,'VariableNames',varNames);


end