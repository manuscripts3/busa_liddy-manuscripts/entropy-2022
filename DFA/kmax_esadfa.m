function [ kmax ] = kmax_esadfa( nmin, nmax )
%KMAX_ESDFA ==============================================================%
%
% ARGUMENTS
% nmin - smallest value of n
% nmax - largest value of n
%
% OUTPUT
% kmax - largest value of k without without empty intervals
%
%=========================================================================%

%initialize timescales
n = nmin:nmax;

%for each k value from nmin to nmax
for k = nmin:nmax
    
    %for each interval (k)
    for i = 1:k
        
        %compute lower and upper bounds
        lb = 10 ^ (log10(nmin) + ((i-1)/k) * log10(nmax/nmin));
        ub = 10 ^ (log10(nmin) + ( i   /k) * log10(nmax/nmin));
        
        %determine whether interval i has observations
        if isempty(find((n >= lb) & (n < ub), 1))
            return; %if the interval is empty, return.
        end
        
    end %end interval
    
    %update kmax
    kmax = k;
    
end %end k

end %end function


