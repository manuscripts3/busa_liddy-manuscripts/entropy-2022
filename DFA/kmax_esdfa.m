function [ kmax ] = kmax_esdfa( nmin, nmax )
%KMAX_ESDFA ==============================================================%
%
% ARGUMENTS
% nmin - smallest value of n
% nmax - largest value of n
%
% OUTPUT
% kmax - largest value of k without duplicate values of n
%
%=========================================================================%

%initially set kmax to nmin
kmax = nmin;

%for each k value from nmin to nmax
for k = nmin:nmax
    
    %timescales
    n = zeros(k,1);
    
    %set the minimum and maximum timescales
    n(1) = nmin;
    n(k) = nmax;
    
    %for each timescale (k-2)
    for i = 2:k-1
        
        %geometric progression of n(i) based on n(i-1)
        n(i) =  round(n(i-1) * 10 ^ ( ( log10(nmax / nmin) ) / (k-1) ));
        
    end %end timescale
    
    %if all elements are unique and n increases monotonically, increment kmax
    if length(unique(n)) == length(n) && all(diff(n)>0)
        kmax =  k;
    else
        return;
    end
    
end %end k

end %end function


