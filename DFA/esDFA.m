function [ a, a_cl, a_w, logn, logf, r2 ] = esDFA( x, nmin, nmax, k, m, INT, PLOT )
%ESDFA Evenly-Spaced Detrended Fluctuation Analysis
%
% INPUT
% x    - signal
% n    - timescales
% k    - number of points
% m    - trend order
% INT - 0 for integration, 1 for no integration
% PLOT - 0 for no plot, 1 for plot
%
% OUTPUT
% a    - alpha exponent
% a_cl - 95% confidence limits of alpha
% a_w  - width of 95 % confidence interval
% logn - log of the timescales, n
% logf - log of the fluctuations, F(n)
% ======================================================================= %

%Preprocessing ===========================================================%

%ensure that the smallest scale is greater than 2*m+1
if min(nmin) < 2*(m+1)
    error('The minimum value of n must be 2x larger than m+1.');
end

%ensure that x is not a matrix
if min(size(x)) ~= 1
    error('Input arguments x and scales must be vectors.');
end

%ensure that x is an array
if size(x,2) == 1
    x = transpose(x);
end

%ensure that nmin and nmax are integers
if sum(mod(nmin,1)) ~= 0 || sum(mod(nmax,1)) ~= 0
    error('The minimum and maximum timescales, nmin and nmax, must be integers.');
end

%Spacing Procedure =======================================================%
%create scale values
n = zeros(1,k);

%set min and max scales
n(1) = nmin;
n(k) = nmax;

%for each timescale
for i = 2:k-1
    
    %calculate timescale i from nmax, nmin, and k
    n(i) = round( n(i-1) * 10 ^ ( ( log10(nmax) - log10(nmin) ) / (k-1) ) );

end %end timescales

%clear loop variable
clear i;

%Detrended Fluctuation Analysis ==========================================%

%omit integration
if INT
    %cumulative sum of x
    X = cumsum( x - mean(x) );
else  
    %demeaned x
    X = x - mean(x);
end

%fluctuation, F(n), for each timescale
F = zeros(1,length(n));

%number of timescales
num_scales = length(n);

%for each timescale
for i = 1 : num_scales
    
    %number of non-overlapping intervals for scale n
    num_ints = floor(length(X) / n(i));
    
    %fluctuation values for each interval
    f = zeros(1,num_ints);
    
    %for each interval
    for j = 1 : num_ints
        
        %indices of the current interval
        interval = ( (j-1) * n(i) ) + 1:j*n(i);
        
        %actual values of the current interval
        x_act = X(interval);
        
        %polynomial fit of order m for the current interval
        p = polyfit(interval, x_act, m);
        
        %predicted values of the current interval
        x_pred = polyval(p, interval);
        
        %MSE of actual - predicted values
        f(j) = mean((x_act - x_pred).^2);
        
    end %end interval
    
    %compute F(n)
    F(1,i) = sqrt(mean(f));
    
end %end scale

%clear loop variables
clear i j;

%Estimate alpha ==========================================================%
logn = log10(n); %convert n to log scale
logf = log10(F); %convert F(n) to log scale
p = polyfit(logn,logf,1); %least-squares fit of log(F(n)) vs. log(n)

%compute statistics
logf_hat = polyval(p, logn); %predicted log(F)
resids = logf - logf_hat; %residuals
mse = sum((resids - mean(resids)) .^ 2) ./ (k - 2); %mean square error
ssx = sum((logn - mean(logn)) .^ 2); %sum of squared error in x
s_a = sqrt(mse / ssx); %standard error of alpha
t = tinv([0.025 0.975], k - 2); %t-score
r2 = sum( ( logf_hat - mean(logf) ) .^ 2) / sum( ( logf - mean(logf)) .^ 2 ); %R-squared

%compute alpha and 95% CL
a = p(1); %alpha exponent, slope of the log(F) vs. log(n) fitted line
a_cl = a + (s_a .* t); %95 confidence limits for alpha
a_w = a_cl(2) - a_cl(1); %width of 95% confidence limits

%Plotting ================================================================%
if PLOT == 1
    
    figure('Color','white');
    scatter(logn,logf, 'k', 'Filled'); %plot log(F(n)) vs. log(n)
    hold on;
    plot(logn, logf_hat, 'k--', 'LineWidth', 1); %overlay trend line
    hold off;
    set(gca,'TickLength', [0 0], 'FontName', 'Times', 'FontSize', 12);
    set(gca,'XTickLabel', num2str(get(gca,'XTick')','%1.1f'));
    set(gca,'YTickLabel', num2str(get(gca,'YTick')','%1.2f'));
    xlabel('log( n )', 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold');
    ylabel('log( F )', 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold');
    
end %end plotting

end %end function

