function [ alpha, alpha_cl, alpha_w, logn, logf, rsquare ] = DFA( x, scales, m, PLOT )
%DFA Detrended Fluctuation Analysis
%
% INPUT
% x - signal
% scales - timescales used to estimate alpha
% m - trend order
% PLOT - 0 for no plot, 1 for plot
%
% OUTPUT
% a - alpha exponent
% logn - log of the timescales
% logf - log of the fluctuations
% ======================================================================= %

%ensure x and scales are not matrices
if min(size(x)) ~= 1 || min(size(scales)) ~= 1
    error('Input arguments signal and scale must be vectors');
end

%ensure x is an array
if size(x,2) == 1;
    x = transpose(x);
end

%check to ensure the smallest scale is 2x larger than the trend order
if min(scales) < 2*(m+1)
    error('The minimum scale must be 2x larger than trend order m+1');
end

%Detrended Fluctuation Analysis ==========================================%
%cumulative sum of x
X = cumsum( x - mean(x) );

%fluctuation, F(n), for each timescale
F = zeros(1,length(scales));

%number of timescales
num_scales = length(scales);

%for each timescale
for i = 1 : num_scales
    
    %number of non-overlapping intervals for scale n
    num_ints = floor(length(X) / scales(i));
    
    %fluctuation values for each interval
    f = zeros(1,num_ints);
    
    %for each interval
    for j = 1 : num_ints
        
        %indices of the current interval
        interval = ( (j-1) * scales(i) ) + 1:j*scales(i);
        
        %actual values of the current interval
        x_act = X(interval);
        
        %polynomial fit of order m for the current interval
        p = polyfit(interval, x_act, m);
        
        %epredicted values of the current interval
        x_pred = polyval(p, interval);
        
        %Variance of actual - predicted values
        %IMPORTANT - f must be MSE because RMSE is not additive
        f(j) = mean((x_act - x_pred).^2);
        
    end %end interval
    
    %Fluctuation, F(n)
    %Average the MSE across intervals and take the square root
    F(1,i) = sqrt(mean(f));
    
end %end scale

%Estimate scaling exponent ===============================================%

%Least-Squres Linear Regression of log(F) on log(n)
logn = log10(scales); %convert n to log scale
logf = log10(F); %convert F(n) to log scale
p = polyfit(logn,logf,1); %least-squares fit of log(F(n)) vs. log(n)

%Compute statistics based on fit
k = length(logn); %number of data points in diffusion plot
logf_hat = polyval(p, logn); %predicted log(F)
resids = logf - logf_hat; %residuals
mse = sum((resids - mean(resids)) .^ 2) ./ (k - 2); %mean square error
ssx = sum((logn - mean(logn)) .^ 2); %sum of squared error in x
s_a = sqrt(mse / ssx); %standard error of alpha
t = tinv([0.025 0.975], k - 2); %t-score
rsquare = sum( ( logf_hat - mean(logf) ) .^ 2) / sum( ( logf - mean(logf)) .^ 2 ); %R^2 of fitted line

%Determine alpha and 95% CL
alpha = p(1); %alpha exponent, slope of the log(F) vs. log(n) fitted line
alpha_cl = alpha + (s_a .* t);
alpha_w = alpha_cl(2) - alpha_cl(1);

%if plotting
if PLOT == 1
    
    figure('Color','white');
    scatter(logn,logf, 'k', 'Filled'); %plot log(F(n)) vs. log(n)
    hold on;
    plot(logn, logf_hat, 'k--', 'LineWidth', 1); %overlay trend line
    hold off;
    set(gca,'TickLength', [0 0], 'FontName', 'Times', 'FontSize', 12);
    set(gca,'XTickLabel', num2str(get(gca,'XTick')','%1.1f'));
    set(gca,'YTickLabel', num2str(get(gca,'YTick')','%1.2f'));
    xlabel('log( n )', 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold');
    ylabel('log( F )', 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold');
    
end %end plotting

end %end function

