% Fit ARFIMA(0,d,0) models to estimate the difference order, d, and DFA to 
% estimate the  scaling exponent, a.

% Clear workspace and command window.
clear; clc;

% Add folders to path
addpath(genpath(fullfile(cd,"ARFIMA")));
addpath(genpath(fullfile(cd,"DFA")));

if ~exist(fullfile(cd,"Results","Results_ARFIMA_Estimation.mat"),'file')

    % Load simulated data
    load(fullfile(pwd,'Data',"Simulations_Estimation.mat"));

    % Difference order, d, and scaling exponent, a.
    dEst = nan(size(simData)); % ARFIMA 
    aEst = nan(size(simData)); % ARFIMA
    aEst2 = nan(size(simData)); % DFA

    % Indicates whether data is nonstationary
    isNS = zeros(size(simData)); % ARFIMA
    isNS2 = zeros(size(simData)); % DFA

    % Obtain parameter estimates for ARFIMA(0,d,0) model and compute the
    % scaling exponent, a, for esaDFA. d estimates were converetd to a.
    for i = 1:numel(simData)
        y = simData{i};
        [aEst2(i), ~, ~, ~, ~, ~ ] = esaDFA( y, 10:floor(length(y)/4), kmax_esadfa( 10, floor(length(y)/4) ), 1, 0, 0 );
        if aEst2(i) > 1
            isNS2(i) = 1;
        end
        model = arfima_estimate(y,'FWHI',[0 0]);
        while model.d(1) > 0.499
            isNS(i) = isNS(i) + 1;
            y = diff(y);
            model = arfima_estimate(y,'FWHI',[0 0]);
        end
        dEst(i) = model.d(1);
        if isNS(i)
            dEst(i) = dEst(i) + isNS(i);
        end
        aEst(i) = (2*dEst(i) + 1) / 2;
    end

    % Save results
    save(fullfile(cd,"Results","Results_ARFIMA_Estimation"),"dEst","aEst","aEst2","isNS","isNS2","d","a","N","nSim","simData");

else

    % Load results
    load(fullfile(pwd,"Results","Results_ARFIMA_Estimation.mat"));

    % Separate results by N
    aEst_250 = squeeze(aEst(:,1,:));
    aEst_500 = squeeze(aEst(:,2,:));
    aEst_1000 = squeeze(aEst(:,3,:));

    % Plot colors
    plotColors = [252 141 98;...
                  102 194 165;...
                  141 160 203] ./ 255;

    % Figure 1 -----------------------------------------------------------%
    figure("Color",'white',"Units","inches","OuterPosition",[3 2 10 4]);
    % Panel A, Errors for short data (N = 250)
    errorPlot(a,aEst_250,1,plotColors);
    title('{\itN} = 250');
    annotation('textbox', [.07, .97, .2, 0],'String','A','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel B, Errors for medium data (N = 500)
    errorPlot(a,aEst_500,2,plotColors);
    title('{\itN} = 500');
    annotation('textbox', [.35, .97, .2, 0],'String','B','FontName','Arial','FontSize',20,'LineStyle','none');

    % Panel C, Errors for long data (N = 1000)
    errorPlot(a,aEst_1000,3,plotColors);
    title('{\itN} = 1000');
    annotation('textbox', [.63, .97, .2, 0],'String','C','FontName','Arial','FontSize',20,'LineStyle','none');

    % Print figure
    print([pwd '\Figures\Figure1_ARFIMA_Estimation'],'-dpng','-r1000');

    % Table 2 ------------------------------------------------------------%
    % Bias and standard deviation
    results = horzcat(a',mean(aEst_250,2)-a',std(aEst_250,0,2),mean(aEst_500,2)-a',std(aEst_500,0,2),mean(aEst_1000,2)-a',std(aEst_1000,0,2));
    varNames = {'Expected a','Bias N=250','Bias N=500','Bias N=1000','SD N=250','SD N=500','SD N=1000'};
    Table2 = array2table(results,'VariableNames',varNames);

end