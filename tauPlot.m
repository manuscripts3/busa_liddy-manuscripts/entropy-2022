function [] = tauPlot(a,tau,plotNum,plotColors)

% Expected a values
aExp = ones(size(tau));
for i = 1:length(a)
    aExp(i,:) = aExp(i,:) .* a(i);
end

% Mean tau
meanTau = mean(tau,2,'omitnan')';

% Create subplot;
subplot(2,3,plotNum); hold on;
swarmchart(aExp',tau', 7.5, 'filled','MarkerFaceColor',plotColors(plotNum,:),'MarkerFaceAlpha',.5,'XJitterWidth',.05);
errorbar(a,meanTau,std(tau,0,2)','k','LineStyle','none','LineWidth',.75,'CapSize',0);
scatter(a,meanTau,'MarkerFaceColor','k','MarkerEdgeColor','none','SizeData',15);
xlim([0 2]); ylim([0 51]); axis square;
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
set(gca,'XTick',0:.25:2); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
set(gca,'YTick', 0:10:50); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.f'));
xlabel('Expected \alpha','FontName','Arial','FontSize', 12);
ylabel('Estimated \tau','FontName', 'Arial','FontSize', 12);

end