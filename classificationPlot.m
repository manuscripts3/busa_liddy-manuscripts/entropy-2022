function [] = classificationPlot(isNS,a,plotNum,plotColors)

% Compute percent classified as nonstationary
isNSPercent = nan(length(a),3);
nSim = size(isNS,3);
for i = 1:length(a)
    isNSPercent(i,:) = sum(isNS(i,:,:) >= 1,3) ./ nSim;
end

% Create subplot
subplot(1,2,plotNum); hold on;
% Error bands (5 %)
plot([min(a)-.1 max(a)+.1],.95*ones(1,2),'k--','LineWidth',1);
plot([min(a)-.1 max(a)+.1],.05*ones(1,2),'k--','LineWidth',1);
% Short data (N = 250)
h1 = plot(a,isNSPercent(:,1),"Color",plotColors(1,:),'LineWidth',1.5);
scatter(a,isNSPercent(:,1),15,'filled',"MarkerFaceColor",plotColors(1,:),"MarkerFaceAlpha",1,'LineWidth',1.5);
% Medium data (N = 500)
h2 = plot(a,isNSPercent(:,2),"Color",plotColors(2,:),'LineWidth',1.5);
scatter(a,isNSPercent(:,2),15,'filled',"MarkerFaceColor",plotColors(2,:),"MarkerFaceAlpha",1,'LineWidth',1.5);
% Long data (N = 1000)
h3 = plot(a,isNSPercent(:,3),"Color",plotColors(3,:),'LineWidth',1.5);
scatter(a,isNSPercent(:,3),15,'filled',"MarkerFaceColor",plotColors(3,:),"MarkerFaceAlpha",1,'LineWidth',1.5);
xlim([min(a)-.1 max(a)+.1]); ylim([-.1 1.1]);
set(gca,'TickLength',[.01 .01],'FontName', 'Arial', 'FontSize', 10,'XColor', 'black','YColor', 'black');
if plotNum == 1
    set(gca,'XTick', min(a)-.1:.25:max(a)+.1); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
elseif plotNum == 2
    set(gca,'XTick', min(a)-.1:.1:max(a)+.1); set(gca,'XTickLabel',num2str(get(gca,'XTick')','%1.2f'));
end
set(gca,'YTick',0:.2:1); set(gca,'YTickLabel',num2str(get(gca,'YTick')','%1.1f'));
xlabel('Expected \alpha','FontName', 'Arial','FontSize', 12);
ylabel('Proportion nonstationary','FontName', 'Arial','FontSize', 12);
legend([h1 h2 h3],{'{\itN} = 250','{\itN} = 500','{\itN} = 1000'},'orientation','vertical','location','east','box','off');

end